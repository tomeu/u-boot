// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2016 Siarhei Siamashka <siarhei.siamashka@gmail.com>
 */

#include <common.h>
#include <spl.h>
#include <asm/gpio.h>
#include <asm/io.h>
#include <linux/libfdt.h>

#ifdef CONFIG_SPL_OS_BOOT
#error CONFIG_SPL_OS_BOOT is not supported yet
#endif

/*
 * This is a very simple U-Boot image loading implementation, trying to
 * replicate what the boot ROM is doing when loading the SPL. Because we
 * know the exact pins where the SPI Flash is connected and also know
 * that the Read Data Bytes (03h) command is supported, the hardware
 * configuration is very simple and we don't need the extra flexibility
 * of the SPI framework. Moreover, we rely on the default settings of
 * the SPI controler hardware registers and only adjust what needs to
 * be changed. This is good for the code size and this implementation
 * adds less than 400 bytes to the SPL.
 *
 * There are two variants of the SPI controller in Allwinner SoCs:
 * A10/A13/A20 (sun4i variant) and everything else (sun6i variant).
 * Both of them are supported.
 *
 * The pin mixing part is SoC specific and only A10/A13/A20/H3/A64/H6 are
 * supported at the moment.
 */

/*****************************************************************************/
/* SUN4I variant of the SPI controller                                       */
/*****************************************************************************/

#define SUN4I_SPI0_CCTL             (0x01C05000 + 0x1C)
#define SUN4I_SPI0_CTL              (0x01C05000 + 0x08)
#define SUN4I_SPI0_RX               (0x01C05000 + 0x00)
#define SUN4I_SPI0_TX               (0x01C05000 + 0x04)
#define SUN4I_SPI0_FIFO_STA         (0x01C05000 + 0x28)
#define SUN4I_SPI0_BC               (0x01C05000 + 0x20)
#define SUN4I_SPI0_TC               (0x01C05000 + 0x24)

#define SUN4I_CTL_ENABLE            BIT(0)
#define SUN4I_CTL_MASTER            BIT(1)
#define SUN4I_CTL_TF_RST            BIT(8)
#define SUN4I_CTL_RF_RST            BIT(9)
#define SUN4I_CTL_XCH               BIT(10)

/*****************************************************************************/
/* SUN6I variant of the SPI controller                                       */
/*****************************************************************************/

#define SUN6I_SPI0                  0x01C68000
#define SUN6I_SPI_CCTL              0x24
#define SUN6I_SPI_GCR               0x04
#define SUN6I_SPI_TCR               0x08
#define SUN6I_SPI_FIFO_STA          0x1C
#define SUN6I_SPI_MBC               0x30
#define SUN6I_SPI_MTC               0x34
#define SUN6I_SPI_BCC               0x38
#define SUN6I_SPI_TXD               0x200
#define SUN6I_SPI_RXD               0x300

#define SUN6I_CTL_ENABLE            BIT(0)
#define SUN6I_CTL_MASTER            BIT(1)
#define SUN6I_CTL_SRST              BIT(31)
#define SUN6I_TCR_XCH               BIT(31)

/*****************************************************************************/
/* SUN50I_H6 variant of the SPI controller                                   */
/*****************************************************************************/

#define SUN50I_H6_SPI0              0x05010000

#define SUN50I_H6_SPI_GATING_REG    (0x03001000 + 0x96C)
#define SUN50I_H6_SPI0_RST          BIT(16)
#define SUN50I_H6_SPI0_GATING_CLK   BIT(0)
#define SUN50I_H6_CCU_SPI0_CLK      (0x03001000 + 0x940)

/*****************************************************************************/

#define CCM_AHB_GATING0             (0x01C20000 + 0x60)
#define CCM_SPI0_CLK                (0x01C20000 + 0xA0)
#define SUN6I_BUS_SOFT_RST_REG0     (0x01C20000 + 0x2C0)

#define AHB_RESET_SPI0_SHIFT        20
#define AHB_GATE_OFFSET_SPI0        20

#define SPI0_CLK_DIV_BY_2           0x1000
#define SPI0_CLK_DIV_BY_4           0x1001

/*****************************************************************************/

/*
 * Allwinner A10/A20 SoCs were using pins PC0,PC1,PC2,PC23 for booting
 * from SPI Flash, H6 using PC0,PC2,PC3,PC5 , everything else is using
 * pins PC0,PC1,PC2,PC3.
 */
static void spi0_pinmux_setup(unsigned int pin_function)
{
	unsigned int pin;

	if (IS_ENABLED(CONFIG_MACH_SUN50I_H6)) {
		sunxi_gpio_set_cfgpin(SUNXI_GPC(0), pin_function);
		sunxi_gpio_set_cfgpin(SUNXI_GPC(2), pin_function);
		sunxi_gpio_set_cfgpin(SUNXI_GPC(3), pin_function);
		sunxi_gpio_set_cfgpin(SUNXI_GPC(5), pin_function);
	} else {
		for (pin = SUNXI_GPC(0); pin <= SUNXI_GPC(2); pin++)
			sunxi_gpio_set_cfgpin(pin, pin_function);

		if (IS_ENABLED(CONFIG_MACH_SUN4I) ||
		    IS_ENABLED(CONFIG_MACH_SUN7I))
			sunxi_gpio_set_cfgpin(SUNXI_GPC(23), pin_function);
		else
			sunxi_gpio_set_cfgpin(SUNXI_GPC(3), pin_function);
	}
}

/*
 * Setup 6 MHz from OSC24M (because the BROM is doing the same).
 */
static void spi0_enable_clock(void)
{
	/* Deassert SPI0 reset on SUN6I */
	if (IS_ENABLED(CONFIG_MACH_SUN50I_H6))
		setbits_le32(SUN50I_H6_SPI_GATING_REG,
			     SUN50I_H6_SPI0_RST);
	else if (IS_ENABLED(CONFIG_SUNXI_GEN_SUN6I))
		setbits_le32(SUN6I_BUS_SOFT_RST_REG0,
			     (1 << AHB_RESET_SPI0_SHIFT));

	/* Open the SPI0 gate */
	if (IS_ENABLED(CONFIG_MACH_SUN50I_H6))
		setbits_le32(SUN50I_H6_SPI_GATING_REG,
			     SUN50I_H6_SPI0_GATING_CLK);
	else
		setbits_le32(CCM_AHB_GATING0, (1 << AHB_GATE_OFFSET_SPI0));

	/* Divide by 4 */
	if (IS_ENABLED(CONFIG_SUNXI_GEN_SUN6I) ||
	    IS_ENABLED(CONFIG_MACH_SUN50I_H6)) {
		u64 spi0_base = IS_ENABLED(CONFIG_MACH_SUN50I_H6) ?
				SUN50I_H6_SPI0 : SUN6I_SPI0;
		writel(SPI0_CLK_DIV_BY_4, spi0_base + SUN6I_SPI_CCTL);
	} else {
		writel(SPI0_CLK_DIV_BY_4, SUN4I_SPI0_CCTL);
	}

	/* 24MHz from OSC24M */
	writel((1 << 31), IS_ENABLED(CONFIG_MACH_SUN50I_H6) ?
				 SUN50I_H6_CCU_SPI0_CLK : CCM_SPI0_CLK);

	if (IS_ENABLED(CONFIG_SUNXI_GEN_SUN6I) ||
	    IS_ENABLED(CONFIG_MACH_SUN50I_H6)) {
		u64 spi0_base = IS_ENABLED(CONFIG_MACH_SUN50I_H6) ?
				     SUN50I_H6_SPI0 : SUN6I_SPI0;

		/* Enable SPI in the master mode and do a soft reset */
		setbits_le32(spi0_base + SUN6I_SPI_GCR,
			     SUN6I_CTL_MASTER |
			     SUN6I_CTL_ENABLE |
			     SUN6I_CTL_SRST);

		/* Wait for completion */
		while (readl(spi0_base + SUN6I_SPI_GCR) & SUN6I_CTL_SRST)
			;
	} else {
		/* Enable SPI in the master mode and reset FIFO */
		setbits_le32(SUN4I_SPI0_CTL, SUN4I_CTL_MASTER |
					     SUN4I_CTL_ENABLE |
					     SUN4I_CTL_TF_RST |
					     SUN4I_CTL_RF_RST);
	}
}

static void spi0_disable_clock(void)
{
	/* Disable the SPI0 controller */
	if (IS_ENABLED(CONFIG_SUNXI_GEN_SUN6I) ||
	    IS_ENABLED(CONFIG_MACH_SUN50I_H6)) {
		u64 spi0_base = IS_ENABLED(CONFIG_MACH_SUN50I_H6) ?
				     SUN50I_H6_SPI0 : SUN6I_SPI0;
		clrbits_le32(spi0_base + SUN6I_SPI_GCR,
			     SUN6I_CTL_MASTER |
			     SUN6I_CTL_ENABLE);
	} else {
		clrbits_le32(SUN4I_SPI0_CTL, SUN4I_CTL_MASTER |
					     SUN4I_CTL_ENABLE);
	}

	/* Disable the SPI0 clock */
	writel(0, IS_ENABLED(CONFIG_MACH_SUN50I_H6) ?
	     SUN50I_H6_CCU_SPI0_CLK : CCM_SPI0_CLK);

	/* Close the SPI0 gate */
	if (IS_ENABLED(CONFIG_MACH_SUN50I_H6))
		clrbits_le32(SUN50I_H6_SPI_GATING_REG,
			     SUN50I_H6_SPI0_GATING_CLK);
	else
		clrbits_le32(CCM_AHB_GATING0, (1 << AHB_GATE_OFFSET_SPI0));

	/* Assert SPI0 reset on SUN6I */
	if (IS_ENABLED(CONFIG_MACH_SUN50I_H6))
		setbits_le32(SUN50I_H6_SPI_GATING_REG,
			     SUN50I_H6_SPI0_RST);
	else if (IS_ENABLED(CONFIG_SUNXI_GEN_SUN6I))
		setbits_le32(SUN6I_BUS_SOFT_RST_REG0,
			     (1 << AHB_RESET_SPI0_SHIFT));
}

static void spi0_init(void)
{
	unsigned int pin_function = SUNXI_GPC_SPI0;

	if (IS_ENABLED(CONFIG_MACH_SUN50I) || IS_ENABLED(CONFIG_MACH_SUN50I_H6))
		pin_function = SUN50I_GPC_SPI0;

	spi0_pinmux_setup(pin_function);
	spi0_enable_clock();
}

static void spi0_deinit(void)
{
	/* New SoCs can disable pins, older could only set them as input */
	unsigned int pin_function = SUNXI_GPIO_INPUT;

	if (IS_ENABLED(CONFIG_SUNXI_GEN_SUN6I) ||
	    IS_ENABLED(CONFIG_MACH_SUN50I_H6))
		pin_function = SUNXI_GPIO_DISABLE;

	spi0_disable_clock();
	spi0_pinmux_setup(pin_function);
}

/*****************************************************************************/

#define SPI_READ_MAX_SIZE 60 /* FIFO size, minus 4 bytes of the header */

static void sunxi_spi0_read_data(u8 *buf, u32 addr, u32 bufsize,
				 ulong spi_ctl_reg,
				 ulong spi_ctl_xch_bitmask,
				 ulong spi_fifo_reg,
				 ulong spi_tx_reg,
				 ulong spi_rx_reg,
				 ulong spi_bc_reg,
				 ulong spi_tc_reg,
				 ulong spi_bcc_reg)
{
	writel(4 + bufsize, spi_bc_reg); /* Burst counter (total bytes) */
	writel(4, spi_tc_reg);           /* Transfer counter (bytes to send) */
	if (spi_bcc_reg)
		writel(4, spi_bcc_reg);  /* SUN6I also needs this */

	/* Send the Read Data Bytes (03h) command header */
	writeb(0x03, spi_tx_reg);
	writeb((u8)(addr >> 16), spi_tx_reg);
	writeb((u8)(addr >> 8), spi_tx_reg);
	writeb((u8)(addr), spi_tx_reg);

	/* Start the data transfer */
	setbits_le32(spi_ctl_reg, spi_ctl_xch_bitmask);

	/* Wait until everything is received in the RX FIFO */
	while ((readl(spi_fifo_reg) & 0x7F) < 4 + bufsize)
		;

	/* Skip 4 bytes */
	readl(spi_rx_reg);

	/* Read the data */
	while (bufsize-- > 0)
		*buf++ = readb(spi_rx_reg);

	/* tSHSL time is up to 100 ns in various SPI flash datasheets */
	udelay(1);
}

static void spi0_read_data(void *buf, u32 addr, u32 len)
{
	u8 *buf8 = buf;
	u32 chunk_len;

	while (len > 0) {
		chunk_len = len;
		if (chunk_len > SPI_READ_MAX_SIZE)
			chunk_len = SPI_READ_MAX_SIZE;

		if (IS_ENABLED(CONFIG_SUNXI_GEN_SUN6I) ||
		    IS_ENABLED(CONFIG_MACH_SUN50I_H6)) {
			u64 base = IS_ENABLED(CONFIG_MACH_SUN50I_H6) ?
					SUN50I_H6_SPI0 : SUN6I_SPI0;
			sunxi_spi0_read_data(buf8, addr, chunk_len,
					     base + SUN6I_SPI_TCR,
						    SUN6I_TCR_XCH,
					     base + SUN6I_SPI_FIFO_STA,
					     base + SUN6I_SPI_TXD,
					     base + SUN6I_SPI_RXD,
					     base + SUN6I_SPI_MBC,
					     base + SUN6I_SPI_MTC,
					     base + SUN6I_SPI_BCC);
		} else {
			sunxi_spi0_read_data(buf8, addr, chunk_len,
					     SUN4I_SPI0_CTL,
					     SUN4I_CTL_XCH,
					     SUN4I_SPI0_FIFO_STA,
					     SUN4I_SPI0_TX,
					     SUN4I_SPI0_RX,
					     SUN4I_SPI0_BC,
					     SUN4I_SPI0_TC,
					     0);
		}

		len  -= chunk_len;
		buf8 += chunk_len;
		addr += chunk_len;
	}
}

static ulong spi_load_read(struct spl_load_info *load, ulong sector,
			   ulong count, void *buf)
{
	spi0_read_data(buf, sector, count);

	return count;
}

/*****************************************************************************/

static int spl_spi_load_image(struct spl_image_info *spl_image,
			      struct spl_boot_device *bootdev)
{
	int ret = 0;
	struct image_header *header;
	header = (struct image_header *)(CONFIG_SYS_TEXT_BASE);

	spi0_init();

	spi0_read_data((void *)header, CONFIG_SYS_SPI_U_BOOT_OFFS, 0x40);

	if (IS_ENABLED(CONFIG_SPL_LOAD_FIT) &&
		image_get_magic(header) == FDT_MAGIC) {
		struct spl_load_info load;

		debug("Found FIT image\n");
		load.dev = NULL;
		load.priv = NULL;
		load.filename = NULL;
		load.bl_len = 1;
		load.read = spi_load_read;
		ret = spl_load_simple_fit(spl_image, &load,
					  CONFIG_SYS_SPI_U_BOOT_OFFS, header);
	} else {
		ret = spl_parse_image_header(spl_image, header);
		if (ret)
			return ret;

		spi0_read_data((void *)spl_image->load_addr,
			       CONFIG_SYS_SPI_U_BOOT_OFFS, spl_image->size);
	}

	spi0_deinit();

	return ret;
}
/* Use priorty 0 to override the default if it happens to be linked in */
SPL_LOAD_IMAGE_METHOD("sunxi SPI", 0, BOOT_DEVICE_SPI, spl_spi_load_image);
